<?php

namespace Drupal\request_cleanup\StackMiddleware;

use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Executes the page caching before the main kernel takes over the request.
 */
class Cleanup implements HttpKernelInterface {

  /**
   * The wrapped HTTP kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The settings object.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  protected static $ignoredGetKeys = ['fbclick', 'utm_source', 'utm_medium', 'utm_campaign'];

  /**
   * Constructs a PageCache object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   * @param \Drupal\Core\Site\Settings $settings
   *   Site settings.
   */
  public function __construct($http_kernel, Settings $settings) {
    $this->httpKernel = $http_kernel;
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE): Response {
    // Only allow page caching on master request.
    if ($type !== static::MASTER_REQUEST || $request->getMethod() !== 'GET') {
      return $this->pass($request, $type, $catch);
    }

    $rebuild_request = FALSE;
    $qs_new = [];
    $ignored_keys = $this->settings->get('request_cleanup.get', self::$ignoredGetKeys);
    $ignored_regex = $this->settings->get('request_cleanup_regex.get', []);
    foreach ($request->query->all() as $key => $value) {
      if (in_array($key, $ignored_keys)) {
        $rebuild_request = TRUE;
        $request->query->remove($key);
      }
      elseif ($ignored_regex) {
        foreach ($ignored_regex as $regex) {
          if (preg_match($regex, $key)) {
            $rebuild_request = TRUE;
            $request->query->remove($key);
            continue;
          }
        }
      }
      else {
        $qs_new[] = $key . '=' . urlencode($value);
      }
    }
    if ($rebuild_request) {
      $qs_new = implode('&', $qs_new);
      $request->server->set('QUERY_STRING', $qs_new);
      $uri_original = $request->server->get('REQUEST_URI');
      // Add new query string if we have non-ignored parameters.
      if ($qs_new) {
        $uri_new = preg_replace('/\?(.*)$/', '?' . $qs_new, $uri_original);
      }
      // Strip the question mark otherwise.
      else {
        $uri_new = preg_replace('/\?(.*)$/', '', $uri_original);
      }
      $request->server->set('REQUEST_URI', $uri_new);
      $request = new Request($request->query->all(), $request->request->all(),
        $request->attributes->all(), $request->cookies->all(),
        $request->files->all(), $request->server->all(), $request->getContent());
    }

    return $this->pass($request, $type, $catch);
  }

  /**
   * Sidesteps the page cache and directly forwards a request to the backend.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object.
   * @param int $type
   *   The type of the request (one of HttpKernelInterface::MASTER_REQUEST or
   *   HttpKernelInterface::SUB_REQUEST)
   * @param bool $catch
   *   Whether to catch exceptions or not
   *
   * @return \Symfony\Component\HttpFoundation\Response $response
   *   A response object.
   */
  protected function pass(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {
    return $this->httpKernel->handle($request, $type, $catch);
  }

}
